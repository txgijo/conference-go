from json import JSONEncoder
from django.db.models import QuerySet
from datetime import datetime


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):             ### if o is an instance of datetime
            return o.isoformat()                ### return o.isoformat()
        else:
            return super().default(o)           ### return super().default(o)


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}                           ### encoders property

    def default(self, o):
        if isinstance(o, self.model):       ### if object to decode is same class as what's in model property
            d = {}                          ### create an empty dictionary that will hold the property names
            if hasattr(o, "get_api_url"):       ### if o has the attribute get_api_url
                d["href"] = o.get_api_url()     ### then add its return value to dictionary with key "href"
            for property in self.properties:    ### as keys and the property values as values, for each name in the properties list
                value = getattr(o, property)    ### get value of property from model instance given just the property name
                if property in self.encoders:           ### encoders property
                    encoder = self.encoders[property]   ### encoders property
                    value = encoder.default(value)      ### encoders property
                d[property] = value         ### put it into dictionary with property name as key
            d.update(self.get_extra_data(o))
            return d                        ### return the dictionary
        else:
            return super().default(o)       ### return super().default(o)  # From the documentation

    def get_extra_data(self, o):
        return {}